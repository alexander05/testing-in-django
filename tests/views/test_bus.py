import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.bus import BusData
from .view import TestCaseView


class BusListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:bus-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:bus-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:bus-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = BusData.load()

        response = self.client.post(reverse('v1:bus-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_substations(self):
        self.authorization()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-list'), {'substations__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_substations_success(self):
        self.authorization()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-list'), {'substations__id': self.data.substation.pk})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'id': self.data.bus.pk,
                    'name': '',

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'protection_devices': [],
                    'voltage_level': self.data.bus.voltage_level.name,
                    'equipment_type': 'СШ voltage_level_10',
                    'equipment_short_name': 'СШ',
                    'owner': None,
                }
            ]
        })

    def test_availability_by_equipment_type_success(self):
        self.authorization()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-list'), {'equipment_type': 'СШ voltage_level_10'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'id': self.data.bus.pk,
                    'name': '',

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'protection_devices': [],
                    'voltage_level': self.data.bus.voltage_level.name,
                    'equipment_type': 'СШ voltage_level_10',
                    'equipment_short_name': 'СШ',
                    'owner': None,
                }
            ]
        })

    def test_empty_by_equipment_type(self):
        self.authorization()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-list'), {'equipment_type': 'another_some_type'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_substation_and_equipment_type(self):
        self.authorization()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-list'), {
            'substations__id': 9999,
            'equipment_type': 'СШ voltage_level_10',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:bus-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'another_some_type',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:bus-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'СШ voltage_level_10',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'id': self.data.bus.pk,
                    'name': '',

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'protection_devices': [],
                    'voltage_level': self.data.bus.voltage_level.name,
                    'equipment_type': 'СШ voltage_level_10',
                    'equipment_short_name': 'СШ',
                    'owner': None,
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:bus-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'id': self.data.bus.pk,
                    'name': '',

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'protection_devices': [],
                    'voltage_level': self.data.bus.voltage_level.name,
                    'equipment_type': 'СШ voltage_level_10',
                    'equipment_short_name': 'СШ',
                    'owner': None,
                }
            ]
        }
                         )


class BusDetailTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:bus-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:bus-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:bus-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:bus-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = BusData.load()

        response = self.client.post(reverse('v1:bus-detail', args=(self.data.bus.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = BusData.load()

        response = self.client.get(reverse('v1:bus-detail', args=(self.data.bus.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'dispatcher_centers': [],
            'id': self.data.bus.pk,
            'name': '',

            'substations': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                },
            ],

            'protection_devices': [],
            'voltage_level': self.data.bus.voltage_level.name,
            'equipment_type': 'СШ voltage_level_10',
            'equipment_short_name': 'СШ',
            'owner': None,
        })

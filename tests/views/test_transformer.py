import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.transformer import TransformerData
from .view import TestCaseView


class TransformerListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:transformer-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:transformer-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:transformer-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = TransformerData.load()

        response = self.client.post(reverse('v1:transformer-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_substations(self):
        self.authorization()

        self.data = TransformerData.load()

        response = self.client.get(reverse('v1:transformer-list'), {'substations__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_substations_success(self):
        self.authorization()

        self.data = TransformerData.load()

        response = self.client.get(reverse('v1:transformer-list'), {'substations__id': self.data.substation.pk})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'auto': True,
                    'connection_types': 'some_connection_types',
                    'equipment_type': 'Т some_voltage',
                    'id': self.data.transformer.pk,
                    'name': '',
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'dispatcher_centers': [],
                    'voltage_level': 'some_voltage',
                    'equipment_short_name': 'Т',
                    'owner': None,
                }
            ]
        })

    def test_availability_by_equipment_type_success(self):
        self.authorization()

        self.data = TransformerData.load()

        response = self.client.get(reverse('v1:transformer-list'), {'equipment_type': 'Т some_voltage'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'auto': True,
                    'connection_types': 'some_connection_types',
                    'equipment_type': 'Т some_voltage',
                    'id': self.data.transformer.pk,
                    'name': '',
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'dispatcher_centers': [],
                    'voltage_level': 'some_voltage',
                    'equipment_short_name': 'Т',
                    'owner': None,
                }
            ]
        })

    def test_empty_by_equipment_type(self):
        self.authorization()

        self.data = TransformerData.load()

        response = self.client.get(reverse('v1:transformer-list'), {'equipment_type': 'another_some_type'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_substation_and_equipment_type(self):
        self.authorization()

        self.data = TransformerData.load()

        response = self.client.get(reverse('v1:transformer-list'), {
            'substations__id': 9999,
            'equipment_type': 'Т some_voltage',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:transformer-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'another_some_type',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:transformer-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'Т some_voltage',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'auto': True,
                    'connection_types': 'some_connection_types',
                    'equipment_type': 'Т some_voltage',
                    'id': self.data.transformer.pk,
                    'name': '',
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'dispatcher_centers': [],
                    'voltage_level': 'some_voltage',
                    'equipment_short_name': 'Т',
                    'owner': None,
                }
            ]
        })

    def test_availability_success(self):

        self.authorization()

        response = self.client.get(reverse('v1:transformer-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = TransformerData.load()

        response = self.client.get(reverse('v1:transformer-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'auto': True,
                    'connection_types': 'some_connection_types',
                    'equipment_type': 'Т some_voltage',
                    'id': self.data.transformer.pk,
                    'name': '',
                    'normal_state': True,
                    'voltage_level': 'some_voltage',

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'equipment_short_name': 'Т',
                    'owner': None,
                    'dispatcher_centers': [],
                }
            ]
        })


class TransformerTest(TestCaseView):

    def test_availability_unauthorized(self):

        response = self.client.get(reverse('v1:transformer-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:transformer-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):

        self.authorization()

        response = self.client.get(reverse('v1:transformer-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:transformer-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = TransformerData.load()

        response = self.client.post(
            reverse('v1:transformer-detail', args=(self.data.transformer.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):

        self.authorization()

        self.data = TransformerData.load()

        response = self.client.get(
            reverse('v1:transformer-detail', args=(self.data.transformer.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'auto': True,
            'connection_types': 'some_connection_types',
            'equipment_type': 'Т some_voltage',
            'id': self.data.transformer.pk,
            'name': '',
            'normal_state': True,

            'substations': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                },
            ],

            'dispatcher_centers': [],
            'voltage_level': 'some_voltage',
            'equipment_short_name': 'Т',
            'owner': None,
        })

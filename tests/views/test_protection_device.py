import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.protection_device import ProtectionDeviceData
from .view import TestCaseView


class ProtectionDeviceListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:protectiondevice-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:protectiondevice-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:protectiondevice-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = ProtectionDeviceData.load()

        response = self.client.post(reverse('v1:protectiondevice-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-list'), {'name': 'some_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.protection_device.pk,
                    "name": "some_title",
                    "model": "some_model",
                    "short_name": None,
                    "usage_start_date": None,
                    "dismantlement_date": None,
                    "status": None,
                    "synchronized": None,
                    "electric_system": None,
                    "owner": None,

                    "dispatcher_center": {
                        "id": self.data.protection_device.dispatcher_center.pk,
                        "name": self.data.protection_device.dispatcher_center.name,
                    },

                    "dispatcher_control": None,
                    "dispatcher_maintenance": None,
                    "trade_name": None,
                    "implementation_type": None,

                    "producer": {
                        "id": self.data.protection_device.producer.pk,
                        "name": self.data.protection_device.producer.name,
                    },

                    "software_version": None,

                    "substation": {
                        "id": self.data.protection_device.substation.pk,
                        "name": self.data.protection_device.substation.name,
                    },

                    "voltage_level": "voltage_level_2",
                    "equipment_type": None,
                    "passport": [],
                    "complexes": [],
                    "functions": [],
                    "normal_state": True,

                    "equipment_base": [
                        {
                            "id": self.data.bus.pk,
                            "name": "",
                            "equipment": "buses",
                        }
                    ],

                    "equipment_status": None,
                },
            ],
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_bus_success(self):
        self.authorization()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-list'), {'buses__id': self.data.bus.pk})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.protection_device.pk,
                    "name": "some_title",
                    "model": "some_model",
                    "short_name": None,
                    "usage_start_date": None,
                    "dismantlement_date": None,
                    "status": None,
                    "synchronized": None,
                    "electric_system": None,
                    "owner": None,

                    "dispatcher_center": {
                        "id": self.data.protection_device.dispatcher_center.pk,
                        "name": self.data.protection_device.dispatcher_center.name,
                    },

                    "dispatcher_control": None,
                    "dispatcher_maintenance": None,
                    "trade_name": None,
                    "implementation_type": None,

                    "producer": {
                        "id": self.data.protection_device.producer.pk,
                        "name": self.data.protection_device.producer.name,
                    },

                    "software_version": None,

                    "substation": {
                        "id": self.data.protection_device.substation.pk,
                        "name": self.data.protection_device.substation.name,
                    },

                    "voltage_level": "voltage_level_2",
                    "equipment_type": None,
                    "passport": [],
                    "complexes": [],
                    "functions": [],
                    "normal_state": True,

                    "equipment_base": [
                        {
                            "id": self.data.bus.pk,
                            "name": "",
                            "equipment": "buses",
                        }
                    ],

                    "equipment_status": None,
                },
            ],
        })

    def test_empty_by_bus(self):
        self.authorization()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-list'), {'buses__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_name_and_bus(self):
        self.authorization()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-list'), {
            'name': 'some_other_title',
            'buses__id': self.data.bus.pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:protectiondevice-list'), {
            'name': 'some_title',
            'buses__id': 9999,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:protectiondevice-list'), {
            'name': 'some_title',
            'buses__id': self.data.bus.pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.protection_device.pk,
                    "name": "some_title",
                    "model": "some_model",
                    "short_name": None,
                    "usage_start_date": None,
                    "dismantlement_date": None,
                    "status": None,
                    "synchronized": None,
                    "electric_system": None,
                    "owner": None,

                    "dispatcher_center": {
                        "id": self.data.protection_device.dispatcher_center.pk,
                        "name": self.data.protection_device.dispatcher_center.name,
                    },

                    "dispatcher_control": None,
                    "dispatcher_maintenance": None,
                    "trade_name": None,
                    "implementation_type": None,

                    "producer": {
                        "id": self.data.protection_device.producer.pk,
                        "name": self.data.protection_device.producer.name,
                    },

                    "software_version": None,

                    "substation": {
                        "id": self.data.protection_device.substation.pk,
                        "name": self.data.protection_device.substation.name,
                    },

                    "voltage_level": "voltage_level_2",
                    "equipment_type": None,
                    "passport": [],
                    "complexes": [],
                    "functions": [],
                    "normal_state": True,

                    "equipment_base": [
                        {
                            "id": self.data.bus.pk,
                            "name": "",
                            "equipment": "buses",
                        }
                    ],

                    "equipment_status": None,
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:protectiondevice-list'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-list'))
        self.assertEquals(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.protection_device.pk,
                    "name": "some_title",
                    "model": "some_model",
                    "short_name": None,
                    "usage_start_date": None,
                    "dismantlement_date": None,
                    "status": None,
                    "synchronized": None,
                    "electric_system": None,
                    "owner": None,

                    "dispatcher_center": {
                        "id": self.data.protection_device.dispatcher_center.pk,
                        "name": self.data.protection_device.dispatcher_center.name,
                    },

                    "dispatcher_control": None,
                    "dispatcher_maintenance": None,
                    "trade_name": None,
                    "implementation_type": None,

                    "producer": {
                        "id": self.data.protection_device.producer.pk,
                        "name": self.data.protection_device.producer.name,
                    },

                    "software_version": None,

                    "substation": {
                        "id": self.data.protection_device.substation.pk,
                        "name": self.data.protection_device.substation.name,
                    },

                    "voltage_level": "voltage_level_2",
                    "equipment_type": None,
                    "passport": [],
                    "complexes": [],
                    "functions": [],
                    "normal_state": True,

                    "equipment_base": [
                        {
                            "id": self.data.bus.pk,
                            "name": "",
                            "equipment": "buses",
                        }
                    ],

                    "equipment_status": None,
                },
            ],
        })


class ProtectionDeviceDetailTest(TestCaseView):

    def test_availability_unauthorized(self):

        response = self.client.get(reverse('v1:protectiondevice-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:protectiondevice-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):

        self.authorization()

        response = self.client.get(reverse('v1:protectiondevice-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:protectiondevice-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = ProtectionDeviceData.load()

        response = self.client.post(reverse('v1:protectiondevice-detail', args=(self.data.protection_device.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):

        self.authorization()

        self.data = ProtectionDeviceData.load()

        response = self.client.get(reverse('v1:protectiondevice-detail', args=(self.data.protection_device.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.protection_device.pk,
            "name": "some_title",
            "model": "some_model",
            "short_name": None,
            "usage_start_date": None,
            "dismantlement_date": None,
            "status": None,
            "synchronized": None,
            "electric_system": None,
            "owner": None,

            "dispatcher_center": {
                "id": self.data.protection_device.dispatcher_center.pk,
                "name": self.data.protection_device.dispatcher_center.name,
            },

            "dispatcher_control": None,
            "dispatcher_maintenance": None,
            "trade_name": None,
            "implementation_type": None,

            "producer": {
                "id": self.data.protection_device.producer.pk,
                "name": self.data.protection_device.producer.name,
            },

            "software_version": None,

            "substation": {
                "id": self.data.protection_device.substation.pk,
                "name": self.data.protection_device.substation.name,
            },

            "voltage_level": "voltage_level_2",
            "equipment_type": None,
            "passport": [],
            "complexes": [],
            "functions": [],
            "normal_state": True,

            "equipment_base": [
                {
                    "id": self.data.bus.pk,
                    "name": "",
                    "equipment": "buses",
                }
            ],

            "equipment_status": None,
        })

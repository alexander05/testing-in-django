import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.generator import GeneratorData
from .view import TestCaseView


class GeneratorListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:generator-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:generator-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:generator-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = GeneratorData.load()

        response = self.client.post(reverse('v1:generator-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_substations(self):
        self.authorization()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-list'), {'substations__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_substations_success(self):
        self.authorization()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-list'), {'substations__id': self.data.substation.pk})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],
                    'dispatcher_centers': [],
                    'id': self.data.generator.pk,
                    'voltage_level': self.data.generator.voltage_level.name,
                    'equipment_type': 'Г voltage_level_10',
                    'equipment_short_name': 'Г',
                    'power_level': 100,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'normal_state': True,
                    'owner': None,
                    'name': '',
                }
            ]
        })

    def test_availability_by_equipment_type_success(self):
        self.authorization()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-list'), {'equipment_type': 'Г voltage_level_10'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],
                    'dispatcher_centers': [],
                    'id': self.data.generator.pk,
                    'voltage_level': self.data.generator.voltage_level.name,
                    'equipment_type': 'Г voltage_level_10',
                    'equipment_short_name': 'Г',
                    'power_level': 100,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'normal_state': True,
                    'owner': None,
                    'name': '',
                }
            ]
        })

    def test_empty_by_equipment_type(self):
        self.authorization()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-list'), {'equipment_type': 'another_some_type'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_substation_and_equipment_type(self):
        self.authorization()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-list'), {
            'substations__id': 9999,
            'equipment_type': 'Г voltage_level_10',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:generator-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'another_some_type',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:generator-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'Г voltage_level_10',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],
                    'dispatcher_centers': [],
                    'id': self.data.generator.pk,
                    'voltage_level': self.data.generator.voltage_level.name,
                    'equipment_type': 'Г voltage_level_10',
                    'equipment_short_name': 'Г',
                    'power_level': 100,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'normal_state': True,
                    'owner': None,
                    'name': '',
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:generator-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],
                    'dispatcher_centers': [],
                    'id': self.data.generator.pk,
                    'voltage_level': self.data.generator.voltage_level.name,
                    'equipment_type': 'Г voltage_level_10',
                    'equipment_short_name': 'Г',
                    'power_level': 100,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'normal_state': True,
                    'owner': None,
                    'name': '',
                }
            ]
        })


class GeneratorDetailTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:generator-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:generator-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:generator-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:generator-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = GeneratorData.load()

        response = self.client.post(reverse('v1:generator-detail', args=(self.data.generator.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = GeneratorData.load()

        response = self.client.get(reverse('v1:generator-detail', args=(self.data.generator.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'breakers': [],
            'dispatcher_centers': [],
            'id': self.data.generator.pk,
            'voltage_level': self.data.generator.voltage_level.name,
            'equipment_type': 'Г voltage_level_10',
            'equipment_short_name': 'Г',
            'power_level': 100,
            'protection_devices': [],

            'substations': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                },
            ],

            'normal_state': True,
            'owner': None,
            'name': '',
        })

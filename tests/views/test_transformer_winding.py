import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.transformer_winding import TransformerWindingData
from .view import TestCaseView


class TransformerWindingListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:transformerwinding-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:transformerwinding-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:transformerwinding-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = TransformerWindingData.load()

        response = self.client.post(reverse('v1:transformerwinding-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_equipment_type_success(self):
        self.authorization()

        self.data = TransformerWindingData.load()

        response = self.client.get(
            reverse('v1:transformerwinding-list'),
            {
                'equipment_type': 'Обмотка voltage_level_11',
            }
        )

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],
                    'connection_type': 'D',
                    'grounded': True,
                    'id': self.data.transformer_winding.pk,
                    'equipment_type': 'Обмотка voltage_level_11',
                    'name': 'CH voltage_level_11 ',
                    'normal_power': 100,
                    'protection_devices': [],
                    'side': 'CH',
                    'transformer': self.data.transformer_winding.transformer.pk,
                    'dispatcher_centers': [],
                    'equipment_short_name': 'Обмотка',
                    'owner': None,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'voltage_level': self.data.transformer_winding.voltage_level.name,
                }
            ]
        })

    def test_empty_by_equipment_type(self):
        self.authorization()

        self.data = TransformerWindingData.load()

        response = self.client.get(reverse('v1:transformerwinding-list'), {'equipment_type': 'another_some_type'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:transformerwinding-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = TransformerWindingData.load()

        response = self.client.get(reverse('v1:transformerwinding-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'connection_type': 'D',
                    'grounded': True,
                    'id': self.data.transformer_winding.pk,
                    'dispatcher_centers': [],
                    'equipment_short_name': 'Обмотка',
                    'owner': None,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'name': 'CH voltage_level_11 ',
                    'normal_power': 100,
                    'side': 'CH',
                    'equipment_type': 'Обмотка voltage_level_11',
                    'transformer': self.data.transformer_winding.transformer.pk,
                    'voltage_level': self.data.transformer_winding.voltage_level.name,
                    'breakers': [],
                    'protection_devices': [],
                }
            ]
        })


class TransformerWindingTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:transformerwinding-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:transformerwinding-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:transformerwinding-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:transformerwinding-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = TransformerWindingData.load()

        response = self.client.post(
            reverse('v1:transformerwinding-detail', args=(self.data.transformer_winding.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = TransformerWindingData.load()

        response = self.client.get(
            reverse('v1:transformerwinding-detail', args=(self.data.transformer_winding.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'breakers': [],
            'connection_type': 'D',
            'grounded': True,
            'id': self.data.transformer_winding.pk,
            'dispatcher_centers': [],
            'equipment_short_name': 'Обмотка',
            'owner': None,

            'substations': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                },
            ],

            'equipment_type': 'Обмотка voltage_level_11',
            'name': 'CH voltage_level_11 ',
            'normal_power': 100,
            'protection_devices': [],
            'side': 'CH',
            'transformer': self.data.transformer_winding.transformer.pk,
            'voltage_level': self.data.transformer_winding.voltage_level.name,
        }
                         )

import json
import os
import shutil

from django.core.cache import cache
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import override_settings
from django.urls import reverse
from rest_framework import status

from core.models import Document
from core.tests.data.document import DocumentData
from .view import TestCaseView
from dbrza import settings

MEDIA_ROOT = os.path.join(settings.MEDIA_ROOT, 'tests')


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class DocumentListTest(TestCaseView):

    def test_unauthorized(self):
        response = self.client.get(reverse('v2:document-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = DocumentData.load()

        response = self.client.get(reverse('v2:document-list'), {'name': 'some_name'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'document_status': None,
                    'document_type': None,
                    'id': self.data.document.pk,
                    'initiatives': [],
                    'name': 'some_name',
                    'library': 'Global',
                    'datafile': 'http://testserver' + self.data.document.datafile.url,
                    'commentary': None,
                    'projects': [],
                    'startup_date': self.data.document.startup_date.isoformat().replace('+00:00', 'Z'),
                    'tags': [],
                    'setpoint_charts': [],
                    'setpoint_values': [],
                    'tasks': [],
                    'templates': [],
                    'last_changed_by': None,
                    'prime': False,
                },
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = DocumentData.load()

        response = self.client.get(reverse('v1:document-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_success(self):
        self.authorization()

        response = self.client.get(reverse('v2:document-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = DocumentData.load()

        response = self.client.get(reverse('v2:document-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'document_status': None,
                    'document_type': None,
                    'id': self.data.document.pk,
                    'initiatives': [],
                    'name': 'some_name',
                    'library': 'Global',
                    'datafile': 'http://testserver' + self.data.document.datafile.url,
                    'commentary': None,
                    'projects': [],
                    'startup_date': self.data.document.startup_date.isoformat().replace('+00:00', 'Z'),
                    'tags': [],
                    'setpoint_charts': [],
                    'setpoint_values': [],
                    'tasks': [],
                    'templates': [],
                    'last_changed_by': None,
                    'prime': False,
                },
            ]
        })

    @classmethod
    def tearDownClass(cls):
        super(DocumentListTest, cls).tearDownClass()
        shutil.rmtree(MEDIA_ROOT)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class DocumentDetailTest(TestCaseView):

    def test_unauthorized(self):
        response = self.client.get(reverse('v1:document-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:document-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:document-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:document-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = DocumentData.load()

        response = self.client.post(reverse('v1:document-detail', args=(self.data.document.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_success(self):
        self.authorization()

        self.data = DocumentData.load()

        response = self.client.get(reverse('v2:document-detail', args=(self.data.document.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'document_status': None,
            'document_type': None,
            'id': self.data.document.pk,
            'initiatives': [],
            'name': 'some_name',
            'commentary': None,
            'library': 'Global',
            'datafile': 'http://testserver' + self.data.document.datafile.url,
            'projects': [],
            'startup_date': self.data.document.startup_date.isoformat().replace('+00:00', 'Z'),
            'tags': [],
            'setpoint_charts': [],
            'setpoint_values': [],
            'tasks': [],
            'templates': [],
            'last_changed_by': None,
            'prime': False,
        })

    @classmethod
    def tearDownClass(cls):
        super(DocumentDetailTest, cls).tearDownClass()
        shutil.rmtree(MEDIA_ROOT)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class DocumentCreateTest(TestCaseView):

    def test_unauthorized(self):
        response = self.client.post(reverse('v1:document-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_upload_success(self):
        self.authorization()

        video = SimpleUploadedFile("file.mp4", b"file_content", content_type="video/mp4")
        response = self.client.post(reverse('v2:document-list'), data={'name': 'test_video', 'datafile': video})

        obj = Document.objects.latest('id')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(response.content), {
            'id': obj.id,
            'name': 'test_video',
            'datafile': 'http://testserver' + obj.datafile.url,
            'commentary': None,
            'document_status': None,
            'document_type': None,
            'external_link': None,
            'library': 'Global',
            'prime': False,
            'tags': [],
        })

    def test_upload_few_files(self):
        self.authorization()

        video = SimpleUploadedFile("file.mp4", b"file_content", content_type="video/mp4")
        response = self.client.post(reverse('v2:document-list'), data={'name': 'test_video', 'datafile': video})

        obj = Document.objects.latest('id')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(response.content), {
            'id': obj.id,
            'name': 'test_video',
            'datafile': 'http://testserver' + obj.datafile.url,
            'commentary': None,
            'document_status': None,
            'document_type': None,
            'external_link': None,
            'library': 'Global',
            'prime': False,
            'tags': [],
        })

        video = SimpleUploadedFile("file_other.mp4", b"file_content_other", content_type="video/mp4")
        response = self.client.post(reverse('v2:document-list'), data={'name': 'test_video_other', 'datafile': video})

        obj = Document.objects.latest('id')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(response.content), {
            'id': obj.id,
            'name': 'test_video_other',
            'commentary': None,
            'datafile': 'http://testserver' + obj.datafile.url,
            'document_status': None,
            'document_type': None,
            'external_link': None,
            'library': 'Global',
            'prime': False,
            'tags': [],
        })

    @classmethod
    def tearDownClass(cls):
        super(DocumentCreateTest, cls).tearDownClass()
        shutil.rmtree(MEDIA_ROOT)

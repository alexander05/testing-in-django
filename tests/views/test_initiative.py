import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.initiative import InitiativeData
from .view import TestCaseView


class InitiativeListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:initiative-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:initiative-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:initiative-list'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.data = InitiativeData.load()

        response = self.client.post(reverse('v1:initiative-list'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = InitiativeData.load()

        response = self.client.get(reverse('v2:initiative-list'), {'name': 'some_name'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'accountable_dispatcher_center': None,
                    'accountable_user': {
                        'id': self.data.initiative.accountable_user.pk,
                        'username': self.data.initiative.accountable_user.username,
                    },
                    'created_by': None,
                    'created_by_dispatcher_center': None,
                    'description': '',
                    'documents': [],
                    'letters': [],
                    'progress': {'count': 0, 'done': 0},
                    'id': self.data.initiative.pk,
                    'initiative_status': None,
                    'name': 'some_name',
                    'participants': [],
                    'target_date': None,
                    'viewed_users': [],
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = InitiativeData.load()

        response = self.client.get(reverse('v2:initiative-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:initiative-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = InitiativeData.load()

        response = self.client.get(reverse('v1:initiative-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'accountable_dispatcher_center': None,
                    'accountable_user': {
                        'id': self.data.initiative.accountable_user.pk,
                        'username': self.data.initiative.accountable_user.username,
                    },
                    'created_by': None,
                    'created_by_dispatcher_center': None,
                    'description': '',
                    'letters': [],
                    'documents': [],
                    'progress': {'count': 0, 'done': 0},
                    'id': self.data.initiative.pk,
                    'initiative_status': None,
                    'name': 'some_name',
                    'participants': [],
                    'target_date': None,
                    'viewed_users': [],
                }
            ]
        })


class InitiativeDetailTest(TestCaseView):

    def test_availability_unauthorized(self):

        response = self.client.get(reverse('v1:initiative-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:initiative-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):

        self.authorization()

        response = self.client.get(reverse('v1:initiative-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:initiative-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = InitiativeData.load()

        response = self.client.post(reverse('v1:initiative-detail', args=(self.data.initiative.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):

        self.authorization()

        self.data = InitiativeData.load()

        response = self.client.get(reverse('v2:initiative-detail', args=(self.data.initiative.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        cache.clear()

        self.assertEqual(json.loads(response.content), {
            'accountable_dispatcher_center': None,
            'accountable_user': {
                'id': self.data.initiative.accountable_user.pk,
                'username': self.data.initiative.accountable_user.username,
            },
            'description': '',
            'created_by': None,
            'created_by_dispatcher_center': None,
            'documents': [],
            'id': self.data.initiative.pk,
            'initiative_status': None,
            'name': 'some_name',
            'progress': {'count': 0, 'done': 0},
            'letters': [],
            'participants': [],
            'target_date': None,
            'viewed_users': [self.user.id],
        })

import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.line import LineData
from .view import TestCaseView


class LineListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:line-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:line-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:line-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = LineData.load()

        response = self.client.post(reverse('v1:line-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_substations(self):
        self.authorization()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-list'), {'substations__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_substations_success(self):
        self.authorization()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-list'), {'substations__id': self.data.substation.pk})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],

                    'dispatcher_centers': [
                        {
                            'id': self.data.dispatcher_center.pk,
                            'name': self.data.dispatcher_center.name,
                        }
                    ],

                    'id': self.data.line.pk,
                    'line_type': '',
                    'name': 'some_title',
                    'normal_state': True,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'owner': None,
                    'equipment_type': 'ЛЭП voltage_level_2',
                    'voltage_level': self.data.voltage_level.name,
                    'equipment_short_name': 'ЛЭП',
                }
            ]
        })

    def test_availability_by_equipment_type_success(self):
        self.authorization()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-list'), {'equipment_type': 'ЛЭП voltage_level_2'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],

                    'dispatcher_centers': [
                        {
                            'id': self.data.dispatcher_center.pk,
                            'name': self.data.dispatcher_center.name,
                        }
                    ],

                    'id': self.data.line.pk,
                    'line_type': '',
                    'name': 'some_title',
                    'normal_state': True,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'owner': None,
                    'equipment_type': 'ЛЭП voltage_level_2',
                    'voltage_level': self.data.voltage_level.name,
                    'equipment_short_name': 'ЛЭП',
                }
            ]
        })

    def test_empty_by_equipment_type(self):
        self.authorization()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-list'), {'equipment_type': 'another_some_type'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_substation_and_equipment_type(self):
        self.authorization()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-list'), {
            'substations__id': 9999,
            'equipment_type': 'ЛЭП voltage_level_2',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:line-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'another_some_type',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:line-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'ЛЭП voltage_level_2',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],

                    'dispatcher_centers': [
                        {
                            'id': self.data.dispatcher_center.pk,
                            'name': self.data.dispatcher_center.name,
                        }
                    ],

                    'id': self.data.line.pk,
                    'line_type': '',
                    'name': 'some_title',
                    'normal_state': True,
                    'protection_devices': [],

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'owner': None,
                    'equipment_type': 'ЛЭП voltage_level_2',
                    'voltage_level': self.data.voltage_level.name,
                    'equipment_short_name': 'ЛЭП',
                }
            ]
        })

    def test_availability_success(self):

        self.authorization()

        response = self.client.get(reverse('v1:line-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'breakers': [],

                    'dispatcher_centers': [
                        {
                            'id': self.data.dispatcher_center.pk,
                            'name': self.data.dispatcher_center.name,
                        },
                    ],

                    'id': self.data.line.pk,
                    'line_type': '',
                    'name': 'some_title',
                    'normal_state': True,
                    'protection_devices': [],

                    'substations':  [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'equipment_type': 'ЛЭП voltage_level_2',
                    'voltage_level': self.data.voltage_level.name,
                    'equipment_short_name': 'ЛЭП',
                    'owner': None,
                }
            ]
        })


class LineDetailTest(TestCaseView):

    def test_availability_unauthorized(self):

        response = self.client.get(reverse('v1:line-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:line-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):

        self.authorization()

        response = self.client.get(reverse('v1:line-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:line-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = LineData.load()

        response = self.client.post(reverse('v1:line-detail', args=(self.data.line.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):

        self.authorization()

        self.data = LineData.load()

        response = self.client.get(reverse('v1:line-detail', args=(self.data.line.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'breakers': [],

            'dispatcher_centers': [
                {
                    'id': self.data.dispatcher_center.pk,
                    'name': self.data.dispatcher_center.name,
                },
            ],

            'id': self.data.line.pk,
            'line_type': '',
            'equipment_type': 'ЛЭП voltage_level_2',
            'name': 'some_title',
            'normal_state': True,
            'protection_devices': [],

            'substations': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                },
            ],

            'voltage_level': self.data.voltage_level.name,
            'equipment_short_name': 'ЛЭП',
            'owner': None,
        })

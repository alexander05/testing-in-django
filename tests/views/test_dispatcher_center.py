import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.dispatcher_center import DispatcherCenterData
from .view import TestCaseView


class DispatcherCenterListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:dispatchercenter-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:dispatchercenter-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:dispatchercenter-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = DispatcherCenterData.load()

        response = self.client.post(reverse('v1:dispatchercenter-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-list'), {'name': 'parent'})

        self.assertEqual(json.loads(response.content),
                         {
                             'count': 1,
                             'next': None,
                             'previous': None,
                             'results': [
                                 {
                                     'id': self.data.parent.pk,
                                     'name': 'parent',
                                     'parent': None,
                                     'breakers': [],
                                     'buses': [],
                                     'generators': [],
                                     'lines': [],
                                     'owner': None,
                                     'projects': [],
                                     'regions': [],
                                     'substations': [],
                                     'transformers': [],
                                 },
                             ]
                         })

    def test_empty_by_name(self):
        self.authorization()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:dispatchercenter-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-list'))
        self.assertEqual(json.loads(response.content),
                         {
                             'count': 2,
                             'next': None,
                             'previous': None,
                             'results': [
                                 {
                                     'id': self.data.parent.pk,
                                     'name': 'parent',
                                     'parent': None,
                                     'breakers': [],
                                     'buses': [],
                                     'generators': [],
                                     'lines': [],
                                     'owner': None,
                                     'projects': [],
                                     'regions': [],
                                     'substations': [],
                                     'transformers': [],
                                 },
                                 {
                                     'id': self.data.child.pk,
                                     'name': 'child',
                                     'parent': self.data.parent.pk,
                                     'breakers': [],
                                     'buses': [],
                                     'generators': [],
                                     'lines': [],
                                     'owner': None,
                                     'projects': [],
                                     'regions': [],
                                     'substations': [],
                                     'transformers': [],
                                 },
                             ]
                         })


class DispatcherCenterTreeTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:dispatchercenter-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:dispatchercenter-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:dispatchercenter-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = DispatcherCenterData.load()

        response = self.client.post(reverse('v1:dispatchercenter-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-tree'), {'name': 'parent'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.parent.pk,
                    'name': 'parent',
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-tree'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:dispatchercenter-tree'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-tree'))
        self.assertEqual(json.loads(response.content), {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.parent.pk,
                    'name': 'parent',
                },
                {
                    'id': self.data.child.pk,
                    'name': 'child',
                },
            ]
        })


class DispatcherCenterDetailTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:dispatchercenter-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:dispatchercenter-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:dispatchercenter-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:dispatchercenter-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = DispatcherCenterData.load()

        response = self.client.post(reverse('v1:dispatchercenter-detail', args=(self.data.child.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.post(reverse('v1:dispatchercenter-detail', args=(self.data.parent.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = DispatcherCenterData.load()

        response = self.client.get(reverse('v1:dispatchercenter-detail', args=(self.data.child.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.child.pk,
            'breakers': [],
            'buses': [],
            'generators': [],
            'lines': [],
            'owner': None,
            'projects': [],
            'regions': [],
            'substations': [],
            'transformers': [],
            'name': 'child',
            'parent': self.data.parent.pk,

        })

        response = self.client.get(reverse('v1:dispatchercenter-detail', args=(self.data.parent.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.parent.pk,
            'name': 'parent',
            'parent': None,
            'breakers': [],
            'buses': [],
            'generators': [],
            'lines': [],
            'owner': None,
            'projects': [],
            'regions': [],
            'substations': [],
            'transformers': [],
        })

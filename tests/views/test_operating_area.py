import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.operating_area import OperatingAreaData
from .view import TestCaseView


class OperatingAreaListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:operatingarea-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:operatingarea-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:operatingarea-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = OperatingAreaData.load()

        response = self.client.post(reverse('v1:operatingarea-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:operatingarea-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = OperatingAreaData.load()

        response = self.client.get(reverse('v1:operatingarea-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.operating_area.pk,
                    'name': 'some_title',
                }
            ]
        }
                         )


class OperatingAreaTreeTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:operatingarea-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:operatingarea-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:operatingarea-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = OperatingAreaData.load()

        response = self.client.post(reverse('v1:operatingarea-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = OperatingAreaData.load()

        response = self.client.get(reverse('v1:operatingarea-tree'), {'name': 'some_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.operating_area.pk,
                    'name': 'some_title',
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = OperatingAreaData.load()

        response = self.client.get(reverse('v1:operatingarea-tree'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:operatingarea-tree'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = OperatingAreaData.load()

        response = self.client.get(reverse('v1:operatingarea-tree'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.operating_area.pk,
                    'name': 'some_title',
                }
            ]
        })


class OperatingAreaTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:operatingarea-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:operatingarea-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:operatingarea-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:operatingarea-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = OperatingAreaData.load()

        response = self.client.post(
            reverse('v1:operatingarea-detail', args=(self.data.operating_area.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = OperatingAreaData.load()

        response = self.client.get(
            reverse('v1:operatingarea-detail', args=(self.data.operating_area.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.operating_area.pk,
            'name': 'some_title',
        })

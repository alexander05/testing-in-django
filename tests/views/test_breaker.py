import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.breaker import BreakerData
from .view import TestCaseView


class BreakerListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:breaker-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:breaker-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:breaker-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = BreakerData.load()

        response = self.client.post(reverse('v1:breaker-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_substations(self):
        self.authorization()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-list'), {'substations__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_substations_success(self):
        self.authorization()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-list'), {'substations__id': self.data.substation.pk})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'generators': [],
                    'id': self.data.breaker.pk,
                    'is_bus_connectivity': True,
                    'lines': [],
                    'protection_devices': [],
                    'name': 'some_title',
                    'owner': None,
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'equipment_type': 'В voltage_level_10',
                    'equipment_short_name': 'В',
                    'transformer_windings': [],
                    'voltage_level': self.data.breaker.voltage_level.name,
                }
            ]
        })

    def test_availability_by_equipment_type_success(self):
        self.authorization()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-list'), {'equipment_type': 'В voltage_level_10'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'generators': [],
                    'id': self.data.breaker.pk,
                    'is_bus_connectivity': True,
                    'lines': [],
                    'protection_devices': [],
                    'name': 'some_title',
                    'owner': None,
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'equipment_type': 'В voltage_level_10',
                    'equipment_short_name': 'В',
                    'transformer_windings': [],
                    'voltage_level': self.data.breaker.voltage_level.name,
                }
            ]
        })

    def test_empty_by_equipment_type(self):
        self.authorization()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-list'), {'equipment_type': 'another_some_type'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_substation_and_equipment_type(self):
        self.authorization()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-list'), {
            'substations__id': 9999,
            'equipment_type': 'В voltage_level_10',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:breaker-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'another_some_type',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:breaker-list'), {
            'substations__id': self.data.substation.pk,
            'equipment_type': 'В voltage_level_10',
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'generators': [],
                    'id': self.data.breaker.pk,
                    'is_bus_connectivity': True,
                    'lines': [],
                    'protection_devices': [],
                    'name': 'some_title',
                    'owner': None,
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'equipment_type': 'В voltage_level_10',
                    'equipment_short_name': 'В',
                    'transformer_windings': [],
                    'voltage_level': self.data.breaker.voltage_level.name,
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:breaker-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [],
                    'generators': [],
                    'id': self.data.breaker.pk,
                    'is_bus_connectivity': True,
                    'lines': [],
                    'protection_devices': [],
                    'name': 'some_title',
                    'owner': None,
                    'normal_state': True,

                    'substations': [
                        {
                            'id': self.data.substation.pk,
                            'name': self.data.substation.name,
                        },
                    ],

                    'equipment_type': 'В voltage_level_10',
                    'equipment_short_name': 'В',
                    'transformer_windings': [],
                    'voltage_level': self.data.breaker.voltage_level.name,
                }
            ]
        })


class BreakerDetailTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:breaker-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:breaker-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:breaker-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:breaker-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = BreakerData.load()

        response = self.client.post(reverse('v1:breaker-detail', args=(self.data.breaker.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = BreakerData.load()

        response = self.client.get(reverse('v1:breaker-detail', args=(self.data.breaker.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'dispatcher_centers': [],
            'generators': [],
            'id': self.data.breaker.pk,
            'is_bus_connectivity': True,
            'lines': [],
            'protection_devices': [],
            'name': 'some_title',
            'owner': None,
            'normal_state': True,

            'substations': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                },
            ],

            'equipment_type': 'В voltage_level_10',
            'equipment_short_name': 'В',
            'transformer_windings': [],
            'voltage_level': self.data.breaker.voltage_level.name,
        })

import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.substation import SubstationData
from .view import TestCaseView


class SubstationListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:substation-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:substation-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:substation-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = SubstationData.load()

        response = self.client.post(reverse('v1:substation-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_region(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-list'), {'regions__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_region_success(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(
            reverse('v1:substation-list'), 
            {
                'regions__id': self.data.substation.regions.first().pk
            },
        )

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'is_station': True,
                    'name': 'some_title',
                    'owner': None,
                    'power_level': 100.0,
                    'regions': [self.data.substation.regions.first().pk],
                    'breakers': [],
                    'buses': [],
                    'dispatcher_centers': [],
                    'voltage_level': self.data.substation.voltage_level.pk,
                    'generators': [],
                }
            ]
        })

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-list'), {'name': 'some_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'is_station': True,
                    'name': 'some_title',
                    'owner': None,
                    'power_level': 100.0,
                    'regions': [self.data.substation.regions.first().pk],
                    'breakers': [],
                    'buses': [],
                    'dispatcher_centers': [],
                    'voltage_level': self.data.substation.voltage_level.pk,
                    'generators': [],
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_name_and_region(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-list'), {
            'name': 'some_other_title',
            'regions__id': self.data.substation.regions.first().pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:substation-list'), {
            'name': 'some_title',
            'regions__id': 9999,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:substation-list'), {
            'name': 'some_title',
            'regions__id': self.data.substation.regions.first().pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'is_station': True,
                    'name': 'some_title',
                    'owner': None,
                    'power_level': 100.0,
                    'regions': [self.data.substation.regions.first().pk],
                    'breakers': [],
                    'buses': [],
                    'dispatcher_centers': [],
                    'voltage_level': self.data.substation.voltage_level.pk,
                    'generators': [],
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:substation-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'is_station': True,
                    'name': 'some_title',
                    'owner': None,
                    'power_level': 100.0,
                    'regions': [self.data.substation.regions.first().pk],
                    'breakers': [],
                    'buses': [],
                    'dispatcher_centers': [],
                    'voltage_level': self.data.substation.voltage_level.pk,
                    'generators': [],
                }
            ]
        })


class SubstationTreeTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:substation-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:substation-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:substation-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = SubstationData.load()

        response = self.client.post(reverse('v1:substation-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_region_(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-tree'), {'regions__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_region_success(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(
            reverse('v1:substation-tree'),
            {
                'regions__id': self.data.substation.regions.first().pk,
            }
        )

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                }
            ]
        })

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-tree'), {'name': 'some_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-tree'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_name_and_region(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-tree'), {
            'name': 'some_other_title',
            'regions__id': self.data.substation.regions.first().pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:substation-tree'), {
            'name': 'some_title',
            'regions__id': 9999,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:substation-tree'), {
            'name': 'some_title',
            'regions__id': self.data.substation.regions.first().pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:substation-tree'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = SubstationData.load()

        response = self.client.get(reverse('v1:substation-tree'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.substation.pk,
                    'name': self.data.substation.name,
                }
            ]
        })


class SubstationTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:substation-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:substation-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:substation-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:substation-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = SubstationData.load()

        response = self.client.post(
            reverse('v1:substation-detail', args=(self.data.substation.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = SubstationData.load()

        response = self.client.get(
            reverse('v1:substation-detail', args=(self.data.substation.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.substation.pk,
            'is_station': True,
            'name': 'some_title',
            'owner': None,
            'power_level': 100.0,
            'generators': [],
            'breakers': [],
            'buses': [],
            'dispatcher_centers': [],
            'voltage_level': self.data.substation.voltage_level.pk,
            'regions': [self.data.substation.regions.first().pk],
        })

import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.owner import OwnerData
from .view import TestCaseView


class OwnerListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:owner-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:owner-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:owner-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = OwnerData.load()

        response = self.client.post(reverse('v1:owner-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = OwnerData.load()

        response = self.client.get(reverse('v1:owner-list'), {'name': 'some_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.owner.pk,
                    'name': 'some_title',
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = OwnerData.load()

        response = self.client.get(reverse('v1:owner-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:owner-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = OwnerData.load()

        response = self.client.get(reverse('v1:owner-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.owner.pk,
                    'name': 'some_title',
                }
            ]
        }
                         )


class OwnerTreeTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:owner-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:owner-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:owner-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = OwnerData.load()

        response = self.client.post(reverse('v1:owner-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = OwnerData.load()

        response = self.client.get(reverse('v1:owner-tree'), {'name': 'some_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.owner.pk,
                    'name': 'some_title',
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = OwnerData.load()

        response = self.client.get(reverse('v1:owner-tree'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:owner-tree'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = OwnerData.load()

        response = self.client.get(reverse('v1:owner-tree'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.owner.pk,
                    'name': 'some_title',
                }
            ]
        })


class OwnerTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:owner-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:owner-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:owner-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:owner-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = OwnerData.load()

        response = self.client.post(
            reverse('v1:owner-detail', args=(self.data.owner.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = OwnerData.load()

        response = self.client.get(
            reverse('v1:owner-detail', args=(self.data.owner.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.owner.pk,
            'name': 'some_title',
        })

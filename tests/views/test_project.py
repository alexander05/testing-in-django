import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.project import ProjectData
from .view import TestCaseView


class ProjectListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:project-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:project-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:project-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = ProjectData.load()

        response = self.client.post(reverse('v1:project-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = ProjectData.load()

        response = self.client.get(reverse('v1:project-list'), {'name': 'some_name'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'accountable_dispatcher_center': None,

                    'accountable_user': {
                        'id': self.data.project.accountable_user.pk,
                        'username': self.data.project.accountable_user.username,
                    },

                    'business_process': {
                        'id': self.data.project.business_process.pk,
                        'name': self.data.project.business_process.name,
                    },

                    'initiative': {
                        'id': self.data.project.initiative.pk,
                        'name': self.data.project.initiative.name,
                    },

                    'last_changed_by': None,
                    'object_to_change': {'name': None},
                    'observers': [],
                    'description': '',
                    'documents': [],
                    'project_setter': None,
                    'project_stage': None,
                    'project_type': None,
                    'id': self.data.project.pk,
                    'name': 'some_name',
                    'target_date': None,
                }
            ]
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = ProjectData.load()

        response = self.client.get(reverse('v1:project-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:project-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = ProjectData.load()

        response = self.client.get(reverse('v1:project-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'accountable_dispatcher_center': None,

                    'accountable_user': {
                        'id': self.data.project.accountable_user.pk,
                        'username': self.data.project.accountable_user.username,
                    },

                    'business_process': {
                        'id': self.data.project.business_process.pk,
                        'name': self.data.project.business_process.name,
                    },

                    'initiative': {
                        'id': self.data.project.initiative.pk,
                        'name': self.data.project.initiative.name,
                    },

                    'last_changed_by': None,
                    'object_to_change': {'name': None},
                    'observers': [],
                    'description': '',
                    'documents': [],
                    'project_setter': None,
                    'project_stage': None,
                    'project_type': None,
                    'id': self.data.project.pk,
                    'name': 'some_name',
                    'target_date': None,
                }
            ]
        })


class ProjectDetailTest(TestCaseView):

    def test_availability_unauthorized(self):

        response = self.client.get(reverse('v1:project-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:project-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):

        self.authorization()

        response = self.client.get(reverse('v1:project-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):

        self.authorization()

        response = self.client.post(reverse('v1:project-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = ProjectData.load()

        response = self.client.post(reverse('v1:project-detail', args=(self.data.project.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):

        self.authorization()

        self.data = ProjectData.load()

        response = self.client.get(reverse('v1:project-detail', args=(self.data.project.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        cache.clear()

        self.assertEqual(json.loads(response.content), {
            'accountable_dispatcher_center': None,

            'accountable_user': {
                'id': self.data.project.accountable_user.pk,
                'username': self.data.project.accountable_user.username,
            },

            'business_process': {
                'id': self.data.project.business_process.pk,
                'name': self.data.project.business_process.name,
            },

            'initiative': {
                'id': self.data.project.initiative.pk,
                'name': self.data.project.initiative.name,
            },

            'last_changed_by': None,
            'object_to_change': {'name': None},
            'observers': [],
            'description': '',
            'documents': [],
            'project_setter': None,
            'project_stage': None,
            'project_type': None,
            'id': self.data.project.pk,
            'name': 'some_name',
            'target_date': None,
        })

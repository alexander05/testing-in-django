import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.voltage_level import VoltageLevelData
from .view import TestCaseView


class VoltageLevelListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:voltagelevel-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:voltagelevel-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:voltagelevel-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = VoltageLevelData.load()

        response = self.client.post(reverse('v1:voltagelevel-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:voltagelevel-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = VoltageLevelData.load()

        response = self.client.get(reverse('v1:voltagelevel-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.voltage_level.pk,
                    'name': 'some_title',
                    'value': 2.0,
                }
            ]
        }
                         )


class VoltageLevelTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:voltagelevel-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:voltagelevel-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:voltagelevel-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:voltagelevel-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = VoltageLevelData.load()

        response = self.client.post(
            reverse('v1:voltagelevel-detail', args=(self.data.voltage_level.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = VoltageLevelData.load()

        response = self.client.get(
            reverse('v1:voltagelevel-detail', args=(self.data.voltage_level.pk,)),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'id': self.data.voltage_level.pk,
            'name': 'some_title',
            'value': 2.0,
        })

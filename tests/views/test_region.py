import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from core.tests.data.region import RegionData
from .view import TestCaseView


class RegionListTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:region-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:region-list'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:region-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = RegionData.load()

        response = self.client.post(reverse('v1:region-list'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_dispatcher_center(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-list'), {'dispatcher_centers__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_dispatcher_center_success(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(
            reverse('v1:region-list'),
            {
                'dispatcher_centers__id': self.data.dispatcher_center.pk,
            }
        )

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [self.data.dispatcher_center.pk],
                    'id': self.data.parent.pk,
                    'name': 'parent',
                    'owners': [],
                    'parent': None,
                    'substations': [],
                },
            ],
        })

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-list'), {'name': 'parent'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [self.data.dispatcher_center.pk],
                    'id': self.data.parent.pk,
                    'name': 'parent',
                    'owners': [],
                    'parent': None,
                    'substations': [],
                },
            ],
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-list'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_name_and_dispatcher_center(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-list'), {
            'name': 'some_other_title',
            'dispatcher_centers__id': self.data.dispatcher_center.pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:region-list'), {
            'name': 'parent',
            'dispatcher_centers__id': 9999,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:region-list'), {
            'name': 'parent',
            'dispatcher_centers__id': self.data.dispatcher_center.pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [self.data.dispatcher_center.pk],
                    'id': self.data.parent.pk,
                    'name': 'parent',
                    'owners': [],
                    'parent': None,
                    'substations': [],
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:region-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-list'))
        self.assertEqual(json.loads(response.content), {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [
                {
                    'dispatcher_centers': [self.data.dispatcher_center.pk],
                    'id': self.data.parent.pk,
                    'name': 'parent',
                    'owners': [],
                    'parent': None,
                    'substations': [],
                },
                {
                    'dispatcher_centers': [],
                    'id': self.data.child.pk,
                    'name': 'child',
                    'owners': [],
                    'parent': self.data.parent.pk,
                    'substations': [],
                },
            ]
        }
                         )


class RegionTreeTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:region-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:region-tree'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:region-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = RegionData.load()

        response = self.client.post(reverse('v1:region-tree'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_by_dispatcher_center(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-tree'), {'dispatcher_centers__id': 9999})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_availability_by_dispatcher_center_success(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(
            reverse('v1:region-tree'),
            {
                'dispatcher_centers__id': self.data.dispatcher_center.pk,
            }
        )

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.parent.pk,
                    'name': 'parent',
                },
            ],
        })

    def test_availability_by_name_success(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-tree'), {'name': 'parent'})

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.parent.pk,
                    'name': 'parent',
                },
            ],
        })

    def test_empty_by_name(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-tree'), {'name': 'some_other_title'})

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

    def test_by_name_and_dispatcher_center(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-tree'), {
            'name': 'some_other_title',
            'dispatcher_centers__id': self.data.dispatcher_center.pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:region-tree'), {
            'name': 'parent',
            'dispatcher_centers__id': 9999,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        response = self.client.get(reverse('v1:region-tree'), {
            'name': 'parent',
            'dispatcher_centers__id': self.data.dispatcher_center.pk,
        })

        self.assertEqual(json.loads(response.content), {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.parent.pk,
                    'name': 'parent',
                }
            ]
        })

    def test_availability_success(self):
        self.authorization()

        response = self.client.get(reverse('v1:region-tree'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': [],
        })

        cache.clear()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-tree'))
        self.assertEqual(json.loads(response.content), {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': self.data.parent.pk,
                    'name': 'parent',
                },
                {
                    'id': self.data.child.pk,
                    'name': 'child',
                },
            ]
        })


class RegionDetailTest(TestCaseView):

    def test_availability_unauthorized(self):
        response = self.client.get(reverse('v1:region-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(reverse('v1:region-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_availability_not_found(self):
        self.authorization()

        response = self.client.get(reverse('v1:region-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_availability_not_allowed(self):
        self.authorization()

        response = self.client.post(reverse('v1:region-detail', args=(1,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.data = RegionData.load()

        response = self.client.post(reverse('v1:region-detail', args=(self.data.child.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.post(reverse('v1:region-detail', args=(self.data.parent.pk,)))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_availability_success(self):
        self.authorization()

        self.data = RegionData.load()

        response = self.client.get(reverse('v1:region-detail', args=(self.data.child.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'dispatcher_centers': [],
            'id': self.data.child.pk,
            'owners': [],
            'substations': [],
            'name': 'child',
            'parent': self.data.parent.pk,
        })

        response = self.client.get(reverse('v1:region-detail', args=(self.data.parent.pk,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(json.loads(response.content), {
            'dispatcher_centers': [self.data.dispatcher_center.pk],
            'id': self.data.parent.pk,
            'owners': [],
            'substations': [],
            'name': 'parent',
            'parent': None,
        })

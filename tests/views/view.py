from django.test import TestCase

from core.models import User
from dbrza.settings import TEST_USER_EMAIL, TEST_USER_PASSWORD, TEST_USER_USERNAME
from django.core.cache import cache


class TestCaseView(TestCase):

    maxDiff = None

    def authorization(self):
        self.user = User.objects.create_superuser(
            username=TEST_USER_USERNAME,
            password=TEST_USER_PASSWORD,
            email=TEST_USER_EMAIL,
        )

        self.login = self.client.login(username=TEST_USER_USERNAME, password=TEST_USER_PASSWORD)

    def setUp(self):
        cache.clear()


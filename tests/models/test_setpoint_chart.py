from django.db import IntegrityError
from django.test import TestCase

from core.factory import UserFactory, SetpointChartFactory
from core.models import User, SetpointChart, SetpointChartStatus, Template
from core.tests.data.setpoint_chart import SetpointChartData


class SetpointChartModelTest(TestCase):

    def setUp(self):
        self.data = SetpointChartData.load()

    def test_name_max_length(self):
        obj = SetpointChart.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_name_unique(self):
        with self.assertRaises(IntegrityError):
            SetpointChartFactory(name='some_title')

    def test_view_string(self):
        obj = SetpointChart.objects.get(name='some_title')
        self.assertEqual('some_title', str(obj))

    def test_delete_user(self):
        user = UserFactory(username='some_user', role__name='some_title')

        obj = SetpointChart.objects.get(name='some_title')
        obj.created_by = user
        obj.last_changed_by = user
        obj.save()

        user = User.objects.get(username='some_user')
        user.delete()

        obj = SetpointChart.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

    def test_delete_setpoint_chart_status(self):
        setpoint_chart_status = SetpointChartStatus.objects.get(name='some_title')
        setpoint_chart_status.delete()

        obj = SetpointChart.objects.get(name='some_title')
        self.assertEqual(obj.status, None)

    def test_delete_template(self):
        template = Template.objects.get(name='some_title')
        template.delete()

        obj = SetpointChart.objects.get(name='some_title')
        self.assertEqual(obj.template, None)

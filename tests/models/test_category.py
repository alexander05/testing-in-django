from django.db import IntegrityError
from django.test import TestCase

from core.factory import UserFactory, CategoryFactory
from core.models import User, Category, Template, CategoryType
from core.tests.data.category import CategoryData


class CategoryModelTest(TestCase):

    def setUp(self):
        self.data = CategoryData.load()

    def test_name_max_length(self):
        obj = Category.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_name_unique(self):
        with self.assertRaises(IntegrityError):
            CategoryFactory(name='some_title')

    def test_view_string(self):
        obj = Category.objects.get(name='some_title')
        self.assertEqual('some_title', str(obj))

    def test_delete_user(self):
        user = UserFactory(username='some_user', role__name='some_title')

        obj = Category.objects.get(name='some_title')
        obj.created_by = user
        obj.last_changed_by = user
        obj.save()

        user = User.objects.get(username='some_user')
        user.delete()

        obj = Category.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

    def test_delete_template(self):
        template = Template.objects.get(name='some_title')
        template.delete()

        obj = Category.objects.get(name='some_title')
        self.assertEqual(obj.template, None)

    def test_delete_category_type(self):
        category_type = CategoryType.objects.get(name='some_title')
        category_type.delete()

        obj = Category.objects.get(name='some_title')
        self.assertEqual(obj.category_type, None)


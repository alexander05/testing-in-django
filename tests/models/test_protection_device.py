from django.db import IntegrityError
from django.test import TestCase

from core.factory import ProtectionDeviceFactory
from core.models import User, ProtectionDevice, DispatcherCenter, Substation, Producer
from core.tests.data.protection_device import ProtectionDeviceData


class ProtectionDeviceModelTest(TestCase):

    def setUp(self):

        self.data = ProtectionDeviceData.load()

    def test_model_max_length(self):
        obj = ProtectionDevice.objects.get(name='some_title')

        model_max_length = obj._meta.get_field('model').max_length
        self.assertEqual(model_max_length, 1000)

        trade_name_max_length = obj._meta.get_field('trade_name').max_length
        self.assertEqual(trade_name_max_length, 1000)

    def test_delete_dispatcher_center(self):

        dispatcher_center = DispatcherCenter.objects.get(name='dispatcher_center_1')
        dispatcher_center.delete()

        obj = ProtectionDevice.objects.get(name='some_title')
        self.assertEqual(obj.dispatcher_center, None)

    def test_name_unique(self):

        with self.assertRaises(IntegrityError):
            ProtectionDeviceFactory(name='some_title')

    def test_view_string(self):

        protection_device = ProtectionDevice.objects.get(name='some_title')
        self.assertEqual('some_title', str(protection_device))

    def test_delete_user(self):

        user = User.objects.get(username='some_user')
        user.delete()

        obj = ProtectionDevice.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

    def test_name_max_length(self):

        obj = ProtectionDevice.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_delete_substation(self):

        substation = Substation.objects.get(name='some_title')
        substation.delete()

        obj = ProtectionDevice.objects.get(name='some_title')
        self.assertEqual(obj.substation, None)

    def test_delete_producer(self):

        producer = Producer.objects.get(name='some_title')
        producer.delete()

        obj = ProtectionDevice.objects.get(name='some_title')
        self.assertEqual(obj.producer, None)

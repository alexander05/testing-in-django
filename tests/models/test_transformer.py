from django.test import TestCase

from core.models import Transformer
from core.tests.data.transformer import TransformerData


class TransformerModelTest(TestCase):

    def setUp(self):

        self.data = TransformerData.load()

    def test_max_length(self):

        transformer = Transformer.objects.get(voltage='some_voltage')

        voltage_max_length = transformer._meta.get_field('voltage').max_length
        self.assertEqual(voltage_max_length, 1000)

        connection_types_max_length = transformer._meta.get_field('connection_types').max_length
        self.assertEqual(connection_types_max_length, 1000)

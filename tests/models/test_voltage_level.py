from django.db import IntegrityError
from django.test import TestCase

from core.factory import UserFactory, VoltageLevelFactory
from core.models import User, VoltageLevel
from core.tests.data.voltage_level import VoltageLevelData


class VoltageLevelModelTest(TestCase):

    def setUp(self):

        self.data = VoltageLevelData.load()

    def test_name_max_length(self):

        obj = VoltageLevel.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_name_unique(self):

        with self.assertRaises(IntegrityError):
            VoltageLevelFactory(name='some_title')

    def test_view_string(self):

        voltage_level = VoltageLevel.objects.get(name='some_title')
        self.assertEqual('some_title', str(voltage_level))

    def test_delete_user(self):
        user = UserFactory(username='some_user', role__name='some_title')

        obj = VoltageLevel.objects.get(name='some_title')
        obj.created_by = user
        obj.last_changed_by = user
        obj.save()

        user = User.objects.get(username='some_user')
        user.delete()

        obj = VoltageLevel.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

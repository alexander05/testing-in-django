from django.db import IntegrityError
from django.test import TestCase

from core.factory import OwnerFactory, UserFactory
from core.models import Owner, User
from core.tests.data.owner import OwnerData


class OwnerModelTest(TestCase):

    def setUp(self):

        self.data = OwnerData.load()

    def test_name_max_length(self):

        obj = Owner.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_name_unique(self):

        with self.assertRaises(IntegrityError):
            OwnerFactory(name='some_title')

    def test_view_string(self):

        voltage_level = Owner.objects.get(name='some_title')
        self.assertEqual('some_title', str(voltage_level))

    def test_delete_user(self):
        user = UserFactory(username='some_user', role__name='some_title')

        obj = Owner.objects.get(name='some_title')
        obj.created_by = user
        obj.last_changed_by = user
        obj.save()

        user = User.objects.get(username='some_user')
        user.delete()

        obj = Owner.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

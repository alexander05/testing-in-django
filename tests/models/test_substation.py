from django.db import IntegrityError
from django.test import TestCase

from core.factory import SubstationFactory
from core.models import Substation, User, VoltageLevel
from core.tests.data.substation import SubstationData


class SubstationModelTest(TestCase):

    def setUp(self):

        self.data = SubstationData.load()

    def test_power_level_is_none(self):
        obj = Substation.objects.get(name='some_title')
        obj.power_level = None

        with self.assertRaises(IntegrityError):
            obj.save()

    def test_is_station_is_none(self):
        obj = Substation.objects.get(name='some_title')
        obj.is_station = None

        with self.assertRaises(IntegrityError):
            obj.save()

    def test_name_max_length(self):

        obj = Substation.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_delete_voltage_level(self):

        voltage_level = VoltageLevel.objects.get(name='voltage_level_1')
        voltage_level.delete()

        obj = Substation.objects.get(name='some_title')
        self.assertEqual(obj.voltage_level, None)

    ''' Parent methods '''
    def test_name_unique(self):

        with self.assertRaises(IntegrityError):
            SubstationFactory(name='some_title')

    def test_view_string(self):

        substation = Substation.objects.get(name='some_title')
        self.assertEqual('some_title', str(substation))

    def test_delete_user(self):

        user = User.objects.get(username='some_user')
        user.delete()

        obj = Substation.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

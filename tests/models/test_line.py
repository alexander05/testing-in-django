from django.db import IntegrityError
from django.test import TestCase

from core.models import Line, User, VoltageLevel
from core.tests.data.line import LineData


class LineModelTest(TestCase):

    def setUp(self):
        self.data = LineData.load()

    def test_line_type_max_length(self):
        obj = Line.objects.get(name='some_title')

        line_type_max_length = obj._meta.get_field('line_type').max_length
        self.assertEqual(line_type_max_length, 3)

    def test_delete_voltage_level(self):
        voltage_level = VoltageLevel.objects.get(name='voltage_level_2')
        voltage_level.delete()

        obj = Line.objects.get(name='some_title')
        self.assertEqual(obj.voltage_level, None)

    ''' Parent methods '''
    def test_name_unique(self):
        with self.assertRaises(IntegrityError):
            Line.objects.create(name='some_title')

    def test_view_string(self):
        line = Line.objects.get(name='some_title')
        self.assertEqual('some_title', str(line))

    def test_delete_user(self):
        user = User.objects.get(username='some_user')
        user.delete()

        obj = Line.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

    def test_name_max_length(self):
        obj = Line.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

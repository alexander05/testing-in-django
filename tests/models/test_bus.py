from django.test import TestCase

from core.models import Bus, VoltageLevel
from core.tests.data.bus import BusData


class BusModelTest(TestCase):

    def setUp(self):

        self.data = BusData.load()

    def test_verbose_name(self):

        bus = Bus.objects.get(aip_id=214892790653557)
        self.assertEqual(bus._meta.verbose_name, 'bus')

    def test_verbose_name_plural(self):

        bus = Bus.objects.get(aip_id=214892790653557)
        self.assertEqual(bus._meta.verbose_name_plural, 'buses')

    def test_delete_voltage_level(self):

        voltage_level = VoltageLevel.objects.get(name='voltage_level_10')
        voltage_level.delete()

        bus = Bus.objects.get(aip_id=214892790653557)
        self.assertEqual(bus.voltage_level, None)

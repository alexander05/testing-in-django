from django.db.models import ProtectedError
from django.test import TestCase

from core.models import User, Role
from core.tests.data.users import UserData


class UserModelTest(TestCase):

    def setUp(self):

        self.data = UserData.load()

    def test_max_length(self):

        obj = User.objects.get(username='technologist')

        first_name_max_length = obj._meta.get_field('first_name').max_length
        self.assertEqual(first_name_max_length, 50)

        last_name_max_length = obj._meta.get_field('last_name').max_length
        self.assertEqual(last_name_max_length, 50)

        patronymic_max_length = obj._meta.get_field('patronymic').max_length
        self.assertEqual(patronymic_max_length, 50)

        position_max_length = obj._meta.get_field('position').max_length
        self.assertEqual(position_max_length, 100)

    def test_search_history_size(self):
        obj = User.objects.get(username='technologist')

        search_history_max_length = obj._meta.get_field('search_history').size
        self.assertEqual(search_history_max_length, 5)

    def test_role_max_length(self):

        obj = Role.objects.get(name='Администратор_технолог')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_role_view_string(self):

        obj = Role.objects.get(name='Администратор_технолог')
        self.assertEqual('Администратор_технолог', str(obj))

    def test_role_delete(self):

        obj = Role.objects.get(name='Администратор_технолог')
        with self.assertRaises(ProtectedError):
            obj.delete()

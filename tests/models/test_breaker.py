from django.db import IntegrityError
from django.test import TestCase

from core.factory import BreakerFactory
from core.models import User, Breaker, VoltageLevel
from core.tests.data.breaker import BreakerData


class BreakerModelTest(TestCase):

    def setUp(self):

        self.data = BreakerData.load()

    def test_delete_voltage_level(self):

        voltage_level = VoltageLevel.objects.get(name='voltage_level_10')
        voltage_level.delete()

        obj = Breaker.objects.get(name='some_title')
        self.assertEqual(obj.voltage_level, None)

    ''' Parent methods '''
    def test_name_unique(self):

        with self.assertRaises(IntegrityError):
            BreakerFactory(name='some_title')

    def test_view_string(self):

        breaker = Breaker.objects.get(name='some_title')
        self.assertEqual('some_title', str(breaker))

    def test_delete_user(self):

        user = User.objects.get(username='some_user')
        user.delete()

        obj = Breaker.objects.get(name='some_title')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

    def test_name_max_length(self):

        obj = Breaker.objects.get(name='some_title')

        name_max_length = obj._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

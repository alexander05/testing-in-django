from django.db import IntegrityError
from django.test import TestCase

from core.models import VoltageLevel, Generator
from core.tests.data.generator import GeneratorData


class GeneratorModelTest(TestCase):

    def setUp(self):

        self.data = GeneratorData.load()

    def test_voltage_is_none(self):

        generator = Generator.objects.get(power_level=100)

        generator.power_level = None
        with self.assertRaises(IntegrityError):
            generator.save()

    def test_delete_voltage_level(self):

        voltage_level = VoltageLevel.objects.get(name='voltage_level_10')
        voltage_level.delete()

        generator = Generator.objects.get(power_level=100)
        self.assertEqual(generator.voltage_level, None)

from django.test import TestCase

from core.models import User, ProtectionDeviceFunction, ProtectionDevice, Function
from core.tests.data.protection_device_function import ProtectionDeviceFunctionData


class ProtectionDeviceFunctionModelTest(TestCase):

    def setUp(self):
        self.data = ProtectionDeviceFunctionData.load()

    def test_delete_dispatcher_center(self):
        protection_device = ProtectionDevice.objects.get(name='some_title')
        protection_device.delete()

        with self.assertRaises(type(ProtectionDeviceFunction.DoesNotExist())):
            ProtectionDeviceFunction.objects.get(function_accounting_group__name='some_name')

    def test_delete_user(self):
        user = User.objects.get(username='some_user')
        user.delete()

        obj = ProtectionDeviceFunction.objects.get(function_accounting_group__name='some_name')

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

    def test_delete_function_type(self):
        function = Function.objects.get(name='some_name')
        function.delete()

        with self.assertRaises(type(ProtectionDeviceFunction.DoesNotExist())):
            ProtectionDeviceFunction.objects.get(function_accounting_group__name='some_name')

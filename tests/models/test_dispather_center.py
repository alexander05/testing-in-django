from django.db import IntegrityError
from django.test import TestCase

from core.factory import UserFactory
from core.models import DispatcherCenter, User
from core.tests.data.dispatcher_center import DispatcherCenterData


class DispatcherCenterModelTest(TestCase):

    def setUp(self):
        self.data = DispatcherCenterData.load()

    def test_delete(self):
        parent = DispatcherCenter.objects.get(name='parent')
        parent.delete()

        child = DispatcherCenter.objects.get(name='child')
        self.assertEqual(child.parent, None)

    ''' Parent methods '''
    def test_name_max_length(self):
        parent = DispatcherCenter.objects.get(name='parent')

        name_max_length = parent._meta.get_field('name').max_length
        self.assertEqual(name_max_length, 1000)

    def test_name_unique(self):
        with self.assertRaises(IntegrityError):
            DispatcherCenter.objects.create(name='parent')

    def test_view_string(self):
        dispatcher_center = DispatcherCenter.objects.get(name='parent')
        self.assertEqual('parent', str(dispatcher_center))

    def test_delete_user(self):
        user = UserFactory(username='some_user', role__name='some_title')

        parent = DispatcherCenter.objects.get(name='parent')
        parent.created_by = user
        parent.last_changed_by = user
        parent.save()

        user = User.objects.get(username='some_user')
        user.delete()

        parent = DispatcherCenter.objects.get(name='parent')

        self.assertEqual(parent.created_by, None)
        self.assertEqual(parent.last_changed_by, None)

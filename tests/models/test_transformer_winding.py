from django.db import IntegrityError
from django.test import TestCase

from core.models import TransformerWinding, User, VoltageLevel, Transformer
from core.tests.data.transformer_winding import TransformerWindingData


class TransformerWindingModelTest(TestCase):

    def setUp(self):

        self.data = TransformerWindingData.load()

    def test_delete_voltage_level(self):

        voltage_level = VoltageLevel.objects.get(name='voltage_level_11')
        voltage_level.delete()

        obj = TransformerWinding.objects.get(normal_power=100)
        self.assertEqual(obj.voltage_level, None)

    def test_delete_transformer(self):

        transformer = Transformer.objects.get(aip_id=214892790653500)
        transformer.delete()

        obj = TransformerWinding.objects.get(normal_power=100)
        self.assertEqual(obj.transformer, None)

    def test_side_is_none(self):
        obj = TransformerWinding.objects.get(normal_power=100)
        obj.side = None

        with self.assertRaises(IntegrityError):
            obj.save()

    def test_normal_power_is_none(self):
        obj = TransformerWinding.objects.get(normal_power=100)
        obj.normal_power = None

        with self.assertRaises(IntegrityError):
            obj.save()

    def test_grounded_is_none(self):
        obj = TransformerWinding.objects.get(normal_power=100)
        obj.grounded = None

        with self.assertRaises(IntegrityError):
            obj.save()

    def test_side_max_length(self):
        obj = TransformerWinding.objects.get(normal_power=100)

        side_max_length = obj._meta.get_field('side').max_length
        self.assertEqual(side_max_length, 3)

    def test_connection_type_max_length(self):
        obj = TransformerWinding.objects.get(normal_power=100)

        connection_type_max_length = obj._meta.get_field('connection_type').max_length
        self.assertEqual(connection_type_max_length, 2)

    def test_delete_user(self):

        user = User.objects.get(username='some_user')
        user.delete()

        obj = TransformerWinding.objects.get(normal_power=100)

        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.last_changed_by, None)

from core.factory import UserFactory, VoltageLevelFactory, DispatcherCenterFactory, SubstationFactory
from core.models import Line


class LineData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.voltage_level = VoltageLevelFactory(name='voltage_level_2', value=2.0)
        obj.dispatcher_center = DispatcherCenterFactory(name='dispatcher_center_2')
        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_3',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.line = Line(
            name='some_title',
            normal_state=True,
            line_type='',
            voltage_level=obj.voltage_level,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.line.save()
        obj.line.substations.add(obj.substation)
        obj.line.dispatcher_centers.add(obj.dispatcher_center)

        return obj

from core.factory import SetpointFactory


class SetpointData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.category = SetpointFactory(
            name='some_title',
            template__name='some_title',
            category__name='some_title',
            setpoint_type__name='some_title',
            measurement_unit__name='some_title',
        )

        return obj

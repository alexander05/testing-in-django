from core.factory import InitiativeFactory


class InitiativeData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.initiative = InitiativeFactory(
            name='some_name',
            accountable_user__username='some_user',
            accountable_user__role__name='some_role',
        )

        return obj

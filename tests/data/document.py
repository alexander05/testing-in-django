from core.factory import DocumentFactory


class DocumentData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.document = DocumentFactory(
            name='some_name',
            datafile__data=b'some_file_content',
        )

        return obj

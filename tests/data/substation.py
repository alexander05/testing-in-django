from core.factory import UserFactory, SubstationFactory, RegionFactory


class SubstationData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_1',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )

        region = RegionFactory(name='some_name')
        obj.substation.regions.add(region)

        return obj

from core.factory import TemplateFactory


class TemplateData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.template = TemplateFactory(
            name='some_title',
            template_type__name='some_title',
        )

        return obj

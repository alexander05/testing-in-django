from core.factory import SetpointValueStatusFactory


class SetpointStatusData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.setpoint_status = SetpointValueStatusFactory(name='some_title')

        return obj

from core.factory import SetpointChartFactory


class SetpointChartData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.setpoint_chart = SetpointChartFactory(
            name='some_title',
            status__name='some_title',
            template__name='some_title',
            template__template_type__name='some_title',
        )

        return obj

from core.factory import DispatcherCenterFactory
from core.models import Region


class RegionData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.parent = Region.objects.create(name='parent')
        obj.child = Region.objects.create(name='child', parent=obj.parent)

        obj.dispatcher_center = DispatcherCenterFactory(name='dispatcher_center_2')
        obj.dispatcher_center.regions.add(obj.parent)

        return obj

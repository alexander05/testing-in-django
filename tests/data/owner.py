from core.factory import OwnerFactory


class OwnerData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.owner = OwnerFactory(name='some_title')

        return obj

from core.factory import ProtectionDeviceComplexFactory


class ProtectionDeviceComplexData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.protection_device_complex = ProtectionDeviceComplexFactory(name='some_title')

        return obj

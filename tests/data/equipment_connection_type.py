from core.factory import EquipmentConnectionTypeFactory


class EquipmentConnectionTypeData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.equipment_connection_type = EquipmentConnectionTypeFactory(name='some_title')

        return obj

from core.factory import RoleFactory


class RoleData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.role = RoleFactory(name='Администратор_технолог')

        return obj

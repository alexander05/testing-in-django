from core.factory import SetpointTypeFactory


class SetpointTypeData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.setpoint_type = SetpointTypeFactory(name='some_title')

        return obj

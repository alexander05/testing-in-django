from core.factory import BusFactory, SubstationFactory, UserFactory


class BusData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.bus = BusFactory(
            voltage_level__name='voltage_level_10',
            voltage_level__value=2.0,
            aip_id=214892790653557,
        )

        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_3',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.bus.substations.add(obj.substation)

        return obj

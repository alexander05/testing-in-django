from core.factory import FunctionAccountingGroupFactory


class FunctionAccountingGroupData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.function_accounting_group = FunctionAccountingGroupFactory(name='some_title')

        return obj

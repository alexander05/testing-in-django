from core.factory import CategoryFactory


class CategoryData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.category = CategoryFactory(
            name='some_title',
            template__name='some_title',
            category_type__name='some_title',
        )

        return obj

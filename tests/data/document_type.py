from core.factory import DocumentTypeFactory


class DocumentTypeData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.document_type = DocumentTypeFactory(name='some_title')

        return obj

from core.factory import SetpointChartStatusFactory


class SetpointChartStatusData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.setpoint_chart_status = SetpointChartStatusFactory(name='some_title')

        return obj

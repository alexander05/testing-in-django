from core.factory import ProducerFactory


class ProducerData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.producer = ProducerFactory(name='some_title')

        return obj

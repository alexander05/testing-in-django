from core.models import DispatcherCenter


class DispatcherCenterData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.parent = DispatcherCenter.objects.create(name='parent')
        obj.child = DispatcherCenter.objects.create(name='child', parent=obj.parent)

        return obj

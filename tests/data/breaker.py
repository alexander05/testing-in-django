from core.factory import UserFactory, BreakerFactory, SubstationFactory


class BreakerData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.breaker = BreakerFactory(
            is_bus_connectivity=True,
            normal_state=True,
            voltage_level__name='voltage_level_10',
            voltage_level__value=2.0,
            name='some_title',
            created_by=obj.user,
            last_changed_by=obj.user,
        )

        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_3',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.breaker.substations.add(obj.substation)

        return obj

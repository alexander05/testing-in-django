from core.factory import PassportFactory, UserFactory


class PassportData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.passport = PassportFactory(
            name='some_title',
            protection_device__model='some_model',
            protection_device__normal_state=True,
            protection_device__dispatcher_center__name='dispatcher_center_1',
            protection_device__name='some_title',
            protection_device__created_by=obj.user,
            protection_device__last_changed_by=obj.user,
            protection_device__substation__name='some_title',
            protection_device__substation__is_station=True,
            protection_device__substation__power_level=100,
            protection_device__substation__voltage_level__name='voltage_level_1',
            protection_device__substation__voltage_level__value=2.0,
            protection_device__substation__created_by=obj.user,
            protection_device__substation__last_changed_by=obj.user,
            protection_device__voltage_level__name='voltage_level_2',
            protection_device__voltage_level__value=2.0,
        )

        return obj

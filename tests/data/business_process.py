from core.factory import BusinessProcessFactory


class BusinessProcessData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.business_process = BusinessProcessFactory(name='some_title')

        return obj

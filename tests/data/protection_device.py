from core.factory import UserFactory, ProtectionDeviceFactory, BusFactory


class ProtectionDeviceData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.protection_device = ProtectionDeviceFactory(
            model='some_model',
            normal_state=True,
            dispatcher_center__name='dispatcher_center_1',
            name='some_title',
            created_by=obj.user,
            last_changed_by=obj.user,
            producer__name='some_title',
            substation__name='some_title',
            substation__is_station=True,
            substation__power_level=100,
            substation__voltage_level__name='voltage_level_1',
            substation__voltage_level__value=2.0,
            substation__created_by=obj.user,
            substation__last_changed_by=obj.user,
            voltage_level__name='voltage_level_2',
            voltage_level__value=2.0,
        )

        obj.bus = BusFactory(
            voltage_level__name='voltage_level_10',
            voltage_level__value=2.0,
            aip_id=214892790653557,
        )

        obj.protection_device.buses.add(obj.bus)

        return obj

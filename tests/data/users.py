from core.factory import UserFactory


class UserData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(
            username='technologist',
            first_name='Петр',
            last_name='Петров',
            patronymic='Петрович',
            position='Технолог',
            role__name='Администратор_технолог',

            search_history=[
                'item_1',
                'item_2',
                'item_3',
            ],
        )

        return obj

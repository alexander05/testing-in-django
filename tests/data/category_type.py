from core.factory import CategoryTypeFactory


class CategoryTypeData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.category_type = CategoryTypeFactory(name='some_title')

        return obj

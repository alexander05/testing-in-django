from core.factory import MeasurementUnitFactory


class MeasurementUnitData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.measurement_unit = MeasurementUnitFactory(name='some_title', secondary_name='some_title')

        return obj

from core.factory import TemplateTypeFactory


class TemplateTypeData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.template_type = TemplateTypeFactory(name='some_title')

        return obj

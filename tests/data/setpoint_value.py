from core.factory import SetpointValueFactory


class SetpointValueData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.category = SetpointValueFactory(
            name='some_title',
            setpoint__name='some_title',
            status__name='some_title',
            additional_attributes='{}',
        )

        return obj

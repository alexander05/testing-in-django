from core.factory import TransformerWindingFactory, UserFactory, VoltageLevelFactory, SubstationFactory


class TransformerWindingData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.transformer_winding = TransformerWindingFactory(
            transformer__auto=True,
            transformer__voltage='voltage_11',
            transformer__connection_types='connection_types_11',
            transformer__aip_id=214892790653500,
            name='some_title',
            normal_power=100,
            connection_type='D',
            grounded=True,
            side='CH',
            created_by=obj.user,
            last_changed_by=obj.user,
        )

        voltage_level = VoltageLevelFactory(
            name='voltage_level_11',
            value=2.0,
        )

        obj.transformer_winding.voltage_level = voltage_level
        obj.transformer_winding.save()

        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_3',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.transformer_winding.substations.add(obj.substation)

        return obj

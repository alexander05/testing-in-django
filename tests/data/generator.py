from core.factory import GeneratorFactory, UserFactory, SubstationFactory


class GeneratorData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.generator = GeneratorFactory(
            voltage_level__name='voltage_level_10',
            voltage_level__value=2.0,
            power_level=100,
        )

        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_3',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.generator.substations.add(obj.substation)

        return obj

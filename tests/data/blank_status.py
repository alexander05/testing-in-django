from core.factory import BlankStatusFactory


class BlankStatusData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.blank_status = BlankStatusFactory(name='some_title')

        return obj

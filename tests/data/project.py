from core.factory import ProjectFactory, UserFactory


class ProjectData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.accountable = UserFactory(username='some_user', role__name='some_title')
        obj.project = ProjectFactory(
            name='some_name',
            initiative__name='some_initiative',
            initiative__accountable_user__username='some_initiative_user',
            initiative__accountable_user__role__name='some_initiative_role',
            accountable_user=obj.accountable,
            business_process__name='some_business_process',
        )

        return obj

from core.factory import FunctionFactory


class FunctionTypeData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.function = FunctionFactory(
            name='some_title',
            ansi='some_ansi',
        )

        return obj

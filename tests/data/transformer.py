from core.factory import TransformerFactory, SubstationFactory, UserFactory


class TransformerData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.user = UserFactory(username='some_user', role__name='some_title')
        obj.transformer = TransformerFactory(
            auto=True,
            voltage='some_voltage',
            connection_types='some_connection_types',
        )

        obj.substation = SubstationFactory(
            name='some_title',
            is_station=True,
            power_level=100,
            voltage_level__name='voltage_level_3',
            voltage_level__value=2.0,
            created_by=obj.user,
            last_changed_by=obj.user,
        )
        obj.transformer.substations.add(obj.substation)

        return obj

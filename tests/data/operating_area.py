from core.factory import OperatingAreaFactory


class OperatingAreaData:

    @staticmethod
    def load():

        obj = type('lamdbaobject', (object,), {})()
        obj.operating_area = OperatingAreaFactory(name='some_title')

        return obj

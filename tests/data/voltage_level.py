from core.factory import VoltageLevelFactory


class VoltageLevelData:

    @staticmethod
    def load():
        obj = type('lamdbaobject', (object,), {})()
        obj.voltage_level = VoltageLevelFactory(name='some_title', value=2.0)

        return obj
